DUNE_PREFIX := _build/default
CLANGML_PPX_CMXA := src/clangml_ppx.cmxa
TESTS_EXE := tests/tests.exe

.PHONY : all
all : $(DUNE_PREFIX)/$(CLANGML_PPX_CMXA)

.PHONY : clean
clean :
	dune clean

.PHONY : install
install : $(DUNE_PREFIX)/clangml_ppx.install
	dune install

.PHONY : tests
tests : $(DUNE_PREFIX)/$(TESTS_EXE)
	$(DUNE_PREFIX)/$(TESTS_EXE)

$(DUNE_PREFIX)/$(CLANGML_PPX_CMXA) : src/ppx_lexer.mll src/clangml_ppx.ml
	dune build $(CLANGML_PPX_CMXA)

$(DUNE_PREFIX)/$(TESTS_EXE) : \
		$(DUNE_PREFIX)/$(CLANGML_PPX_CMXA) \
		tests/tests.ml
	dune build $(TESTS_EXE)

$(DUNE_PREFIX)/clangml_ppx.install : \
		$(DUNE_PREFIX)/$(CLANGML_PPX_CMXA) \
		clangml_ppx.opam
	dune build @install
